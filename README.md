first_sysrpl
============

First HP50G Sys RPL program.

Validate Mac/Linux toolchain for compiling SYS RPL programs using **hptools**.

Please see https://www.hpcalc.org/ for **hptools**.

Makefile auto detects Mac or Linux platform, so **make clean && make** should just
work out of the box.

``` text

    * Include the header file KEYDEFS.H, which defines words
    * like kcUpArrow at physical key numbers.
    *
    INCLUDE KEYDEFS.H
    *
    * Include the eight characters needed for binary download
    *
    ASSEMBLE
      NIBASC	/HPHP49-C/
    RPL
    *
    * Begin the secondary
    *
    ::
      CK1NOLASTWD           ( check if there is an argument )
      CK&DISPATCH1          ( check if it is a real number )
      BINT1 ::              ( if it is )
        %2 %^               ( square the radius )
        %PI                 ( put PI in the stack )
        %*                  ( and multiply )
      ;
    ;


```

Use Kermit in Binary Mode and **send** the file **first** to the calculator.

Place a radius on the stack and press **first**.

Awesome, it works !!

If you are using an SD card, just copy **first** on to the SD card and insert it into the H50G.
Then copy the program from SD card to say, HOME. Then execute as normal.


MD5 of recent build matches original md5. Thats a good sign !!

``` text
✔ ~/repos/first [master|…1⚑ 1]
19:58 $ md5sum first
27bedcb7b3022eae2b077618e12a2683  first

~/repos/first [master|…1⚑ 1]
19:58 $ md5sum FIRST_original
27bedcb7b3022eae2b077618e12a2683  FIRST_original
✔ ~/repos/first [master|…1⚑ 1]

```

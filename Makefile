#
# Simple make file for HP 50G toolchain on Linux/Mac
#
# Author: Frank Singleton (b17flyboy@gmail.com)
#
# Can use Mac or Linux build of hptools
#
# Linux hptools was built on Fedora 28 x86_64
# Darwin hptools was built on MacOS 10.13.4
#

# for platform detection.
UNAME=$(shell uname)
$(info UNAME is $(UNAME))

PRG            = first

# From Fedora 28 /etc/fstab, we have mounted x49gp emulator's sdcard as follows
# /home/frank/repos/x49gp/sdcard on /mnt/x49gp type vfat ....
# This allows us to make dl, copying the executable program onto the SD card.
# Convenient for testing inside x49gp emulator running on Fedora 28.
#
DL_AREA		= /mnt/x49gp

VIEW_BIN	= hexdump -Cv

# You should not have to change anything below here.

ifeq ($(UNAME),Darwin)
  $(info Using Darwin executables for hp tools)
  COMPILER	= ./mac/rplcomp
  ASSEMBLER	= ./mac/sasm
  LINKER	= ./mac/sload
  MAKEROM       = ./mac/makerom
else ifeq ($(UNAME),Linux)
  $(info Using Linux executables for hp tools)
  COMPILER	= ./linux/rplcomp
  ASSEMBLER	= ./linux/sasm
  LINKER	= ./linux/sload
else
  $(error Unsupported platform.)
endif

CFLAGS		=
AFLAGS		= -e
LFLAGS		= -H

ENTRIES		= suprom49.a


# simple target for hp50 programs
all:
	$(COMPILER) $(CFLAGS) $(PRG).s $(PRG).a
	$(ASSEMBLER) $(AFLAGS) $(PRG).a
	$(ASSEMBLER) $(AFLAGS) $(ENTRIES)
	$(LINKER) $(LFLAGS) $(PRG).m


clean:
	rm -rf *.o $(PRG) *.l *.lr $(PRG).a *~


# copy to sdcard mounted via loop used by x49gp emulator etc
dl:
	cp $(PRG) $(DL_AREA)

view_bin:
	$(VIEW_BIN) $(PRG)


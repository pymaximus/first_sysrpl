Saturn Assembler                                       Wed Sep 11 13:47:10 2019
V3.0.8 (12/06/2002)                                      first.a        Page    1

    1             * File  first.s 1
    2
    3             * Include the header file KEYDEFS.H, which defines words
    4             * like kcUpArrow at physical key numbers.
    5             *
    6             * File  first.s 5
    7             *|| Reading from KEYDEFS.H
    8             * ASSEMBLE
    9                     CLRLIST ALL
   10             * RPL
   11             * ASSEMBLE
   13             * RPL
   14             * RPL
   15             * insure RPL-mode exit
   16             *|| Resuming file first.s at line 5
   17             * ASSEMBLE
   18             * File  first.s 10
   19 00000 84058         NIBASC  /HPHP49-C/
            40543
            93D234
   20             * File  first.s 11
   21             * RPL
   22             *
   23             * Begin the secondary
   24             *
   25             * File  first.s 15
   26 00010 00000         CON(5)  =DOCOL
   27             * File  first.s 16
   28 00015 00000         CON(5)  =CK1NOLASTWD
   29             * check if there is an argument
   30             * File  first.s 17
   31 0001A 00000         CON(5)  =CK&DISPATCH1
   32             * check if it is a real number
   33             * File  first.s 18
   34 0001F 00000         CON(5)  =BINT1
   35 00024 00000         CON(5)  =DOCOL
   36             * if it is
   37             * File  first.s 19
   38 00029 00000         CON(5)  =%2
   39 0002E 00000         CON(5)  =%^
   40             * square the radius
   41             * File  first.s 20
   42 00033 00000         CON(5)  =%PI
   43             * put PI in the stack
   44             * File  first.s 21
   45 00038 00000         CON(5)  =%*
   46             * and multiply
   47             * File  first.s 22
   48 0003D 00000         CON(5)  =SEMI
   49             * File  first.s 23
   50 00042 00000         CON(5)  =SEMI
   51             * File  first.s 24
   52
   53             * File  first.s 25
   54
   55             * File  first.s 26
   56
Saturn Assembler                                       Wed Sep 11 13:47:10 2019
V3.0.8 (12/06/2002)   Symbol Table                       first.a        Page    2

 %*                                Ext                   -    45
 %2                                Ext                   -    38
 %PI                               Ext                   -    42
 %^                                Ext                   -    39
 BINT1                             Ext                   -    34
 CK&DISPATCH1                      Ext                   -    31
 CK1NOLASTWD                       Ext                   -    28
 DOCOL                             Ext                   -    26    35
 SEMI                              Ext                   -    48    50
Saturn Assembler                                       Wed Sep 11 13:47:10 2019
V3.0.8 (12/06/2002)   Statistics                         first.a        Page    3

Input Parameters

  Source file name is first.a

  Listing file name is first.l

  Object file name is first.o

  Flags set on command line
    None

Warnings:

  None

Errors:

  None

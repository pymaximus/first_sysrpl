
* Include the header file KEYDEFS.H, which defines words
* like kcUpArrow at physical key numbers.
*
INCLUDE KEYDEFS.H
*
* Include the eight characters needed for binary download
*
ASSEMBLE
	NIBASC	/HPHP49-C/
RPL
*
* Begin the secondary
*
::
  CK1NOLASTWD           ( check if there is an argument )
  CK&DISPATCH1          ( check if it is a real number )
  BINT1 ::              ( if it is )
    %2 %^               ( square the radius )
    %PI                 ( put PI in the stack )
    %*                  ( and multiply )
  ;               
;     
       

        
